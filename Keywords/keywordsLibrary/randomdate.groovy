package keywordsLibrary
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;
import java.util.Calendar;
public class randomdate {
	@Keyword
	getRandomDate() {
		Long max =0L;
		Long min =100000000000L;
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		for (int i = 0; i <= 10; i++) {
			Random r = new Random();
			Long randomLong=(r.nextLong() % (max - min)) + min;
			Date dt =new Date(randomLong);
			println("Generated Long:"+ randomLong);
			println("Date generated from long: "+spf.format(dt))
			return(spf.format(dt))
		}
	}

	@Keyword
	getMajorDateInput() {
		Calendar instance = Calendar.getInstance();
		instance.setTime(new Date());
		instance.add(Calendar.YEAR, -18);
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormat.format(instance.getTime()).toString()
		return date
	}

	@Keyword
	getMinorDateInput() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateobj = new Date();
		return(df.format(dateobj));
	}
	@Keyword
	getDateInput() {
		DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd h:mm aa");
		Date obj = new Date();
		return(df1.format(obj));
	}
}