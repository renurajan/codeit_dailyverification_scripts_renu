import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.concurrent.ConcurrentHashMap.SearchEntriesTask

import org.junit.After
import org.testng.annotations.Test

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
//verify carebook icon present
WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CareBook/CareBookIcon'),10,FailureHandling.CONTINUE_ON_FAILURE)

//send patient firstname  input for carebook search
test.log(LogStatus.INFO, "Search patient firstname and lastname in carebook")
WebUI.clearText(findTestObject('Object Repository/Applications/CareBook/CB_Search_FirstName'),FailureHandling.STOP_ON_FAILURE)
WebUI.sendKeys(findTestObject('Object Repository/Applications/CareBook/CB_Search_FirstName'),patientFirstName,FailureHandling.STOP_ON_FAILURE)

//send patient lastname input for carebook search
WebUI.clearText(findTestObject('Object Repository/Applications/CareBook/CB_Search_LastName'), FailureHandling.STOP_ON_FAILURE)
WebUI.sendKeys(findTestObject('Object Repository/Applications/CareBook/CB_Search_LastName'), patientLastName, FailureHandling.STOP_ON_FAILURE)

//click carebook search icon
WebUI.waitForElementVisible(findTestObject('Object Repository/Applications/CareBook/CB_SearchButton'),10, FailureHandling.STOP_ON_FAILURE) 
WebUI.click(findTestObject('Object Repository/Applications/CareBook/CB_SearchButton'),FailureHandling.STOP_ON_FAILURE)

WebUI.delay(7)

//select the patient from carebook search
TestObject patientObjectInSearchListWindow = findTestObject('Base/commanXpath')
patientObjectInSearchListWindow.findProperty('xpath').setValue("//div[contains(@md-virtual-repeat,'carebookSearchResult')]/div/div[normalize-space(text()='"+patientLastName+"')]//following-sibling::div[normalize-space(text()='"+patientFirstName+"')]//following-sibling::div[normalize-space(text()='"+patientID+"')]")
WebUI.doubleClick(patientObjectInSearchListWindow, FailureHandling.STOP_ON_FAILURE)

//handling popup for other patient 			
if(WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CareBook/CB_OtherOrgPatient_PopUp'), 5, FailureHandling.OPTIONAL)){
	WebUI.click(findTestObject('Object Repository/Applications/CareBook/CB_OtherOrgPatient_PopUp'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(10)
}

//switch to carebook home page
WebUI.switchToFrame(findTestObject('Object Repository/Applications/CareBook/CarebookHomePage/CB_CBHP_SwitchToFrame'), 50, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(10)

//refresh carebook home page
WebUI.verifyElementPresent(findTestObject('Applications/CareBook/CarebookHomePage/CB_CBHP_Options'),10, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Applications/CareBook/CarebookHomePage/CB_CBHP_Options'), FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementPresent(findTestObject('Applications/CareBook/CarebookHomePage/CB_CBHP_Options_Refresh'), 10, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Applications/CareBook/CarebookHomePage/CB_CBHP_Options_Refresh'), FailureHandling.STOP_ON_FAILURE)

//verify patient details displayed in carebook home page
test.log(LogStatus.INFO, "Verify Patient details displayed in Home page")
TestObject patientdetails = findTestObject('Base/commanXpath')
patientdetails.findProperty('xpath').setValue("//div[contains(@id,'patientDetails')]/label[contains(text(),'"+patientID+"')]")
WebUI.verifyElementPresent(patientdetails,10,FailureHandling.STOP_ON_FAILURE)

//switch homepage
WebUI.switchToDefaultContent()
WebUI.delay(3)
WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)

GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
test.log(LogStatus.PASS,"TestCase is executed successfully")
	}
	}catch(StepFailedException e){
		test.log(LogStatus.FAIL, e.message)
		CustomKeywords.'reports.extentReports.takeScreenshot'(test)
		throw e
	}
	





















