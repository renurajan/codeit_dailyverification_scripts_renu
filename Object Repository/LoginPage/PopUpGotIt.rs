<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PopUpGotIt</name>
   <tag></tag>
   <elementGuidId>4a7f47fa-526e-4819-897f-1d2828f1d42e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),&quot;Got it&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),&quot;Got it&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
