<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_SP_CreateDelegationPopUpLabel</name>
   <tag></tag>
   <elementGuidId>733ea2bf-4fba-4ade-ba1d-55ddb0173399</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[contains(text(),'Are you sure want to create a delegate ')]</value>
   </webElementProperties>
</WebElementEntity>
