<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Care Plan Gap Report</name>
   <tag></tag>
   <elementGuidId>e6082959-5497-4a82-8b41-6e37b74eda33</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div/div[text()='Careplan Gap Report']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective</value>
   </webElementProperties>
</WebElementEntity>
