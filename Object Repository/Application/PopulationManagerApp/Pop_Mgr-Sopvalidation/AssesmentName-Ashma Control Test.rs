<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssesmentName-Ashma Control Test</name>
   <tag></tag>
   <elementGuidId>784e3c7e-7f4e-4885-8cde-6dbed812c1ab</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Assessment Name']/following::select[1]/option[@value='Asthma Control Test']</value>
   </webElementProperties>
</WebElementEntity>
