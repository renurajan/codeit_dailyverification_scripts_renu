<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EncounterStartDate</name>
   <tag></tag>
   <elementGuidId>afef483f-cbfc-4191-be67-e6a12ac68f6f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Encounter Start Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
