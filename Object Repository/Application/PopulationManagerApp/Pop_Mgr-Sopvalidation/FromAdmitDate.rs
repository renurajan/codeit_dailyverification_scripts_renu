<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FromAdmitDate</name>
   <tag></tag>
   <elementGuidId>bb3c9c42-34d5-4611-954d-6be20163d9a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='From Admit Date']/following::input[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='From Admit Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
