<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PM_Common_LoadingSymbol</name>
   <tag></tag>
   <elementGuidId>9f3e65bd-739d-4d61-bee7-b74fc169e2d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='spinner large-spinner']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='spinner large-spinner']</value>
   </webElementProperties>
</WebElementEntity>
