<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_LastName</name>
   <tag></tag>
   <elementGuidId>8627c9a7-ee10-417e-8398-710d7307b08d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='tabappCareteams']//label[text()=&quot;Last Name&quot;]/../following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='tabappCareteams']//label[text()=&quot;Last Name&quot;]/../following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
