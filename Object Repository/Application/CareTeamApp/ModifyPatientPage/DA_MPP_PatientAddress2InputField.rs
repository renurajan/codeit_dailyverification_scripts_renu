<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_PatientAddress2InputField</name>
   <tag></tag>
   <elementGuidId>f4a27f9d-8f5b-4ff7-8f6e-2ff42a51a94b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'streetAddress2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>streetAddress2</value>
   </webElementProperties>
</WebElementEntity>
