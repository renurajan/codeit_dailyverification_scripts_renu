<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_PatientDateOfBirthInputField</name>
   <tag></tag>
   <elementGuidId>049cc660-1013-478d-b149-0c0c3ee78b00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'dateOfBirthDateItem_dateTextField']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>dateOfBirthDateItem_dateTextField</value>
   </webElementProperties>
</WebElementEntity>
