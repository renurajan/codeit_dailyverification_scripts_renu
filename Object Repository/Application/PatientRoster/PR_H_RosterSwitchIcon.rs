<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PR_H_RosterSwitchIcon</name>
   <tag></tag>
   <elementGuidId>654f95fa-a80b-45b6-b42d-8d591124b3b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div/i[text()='arrow_back']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div/i[text()='arrow_back']</value>
   </webElementProperties>
</WebElementEntity>
