<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_UUP_FirstNameInput</name>
   <tag></tag>
   <elementGuidId>6cd44e8d-a53c-4af0-ab86-49b54eb732cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
