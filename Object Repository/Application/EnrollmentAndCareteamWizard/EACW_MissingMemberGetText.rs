<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_MissingMemberGetText</name>
   <tag></tag>
   <elementGuidId>97bef27a-8ad6-4de4-81dc-20b07132e1c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='ng-binding ng-scope']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='ng-binding ng-scope']</value>
   </webElementProperties>
</WebElementEntity>
