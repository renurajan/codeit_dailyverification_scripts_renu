<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_ConsentAdvisoryPopupYesButton</name>
   <tag></tag>
   <elementGuidId>6a0f6913-b8a3-432a-b71d-a58b2d978969</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-dialog[contains(@class, &quot;consent-advisory-dialog&quot;)]/md-dialog-actions/button/span[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
</WebElementEntity>
