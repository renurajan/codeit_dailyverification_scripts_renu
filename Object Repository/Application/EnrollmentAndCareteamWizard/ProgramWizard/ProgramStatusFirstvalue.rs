<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProgramStatusFirstvalue</name>
   <tag></tag>
   <elementGuidId>096a244c-9509-4ed4-90ea-bfadaae4c4b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//md-option[@ng-repeat=&quot;opt in ctrl.data.program.statuses&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option[@ng-repeat=&quot;opt in ctrl.data.program.statuses&quot;]</value>
   </webElementProperties>
</WebElementEntity>
