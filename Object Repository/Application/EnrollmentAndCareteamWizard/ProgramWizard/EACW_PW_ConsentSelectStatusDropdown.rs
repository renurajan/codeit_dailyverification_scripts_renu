<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ConsentSelectStatusDropdown</name>
   <tag></tag>
   <elementGuidId>d9b4f7f5-6e37-4deb-890d-d39a29c43e14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-select[@name='consent']//div[text()='Select Status']</value>
   </webElementProperties>
</WebElementEntity>
