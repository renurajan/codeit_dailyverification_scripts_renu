<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_ConsentAdvisoryPopupYesButton</name>
   <tag></tag>
   <elementGuidId>8efe9336-b99b-4ef0-ad04-7452ef913a9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-dialog[contains(@class, &quot;consent-advisory-dialog&quot;)]/md-dialog-actions/button/span[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
</WebElementEntity>
