<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_NoDateRequiredRadioButton</name>
   <tag></tag>
   <elementGuidId>d22d9711-c6bc-4861-baf8-c7a06d9d0ecc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=&quot;noteRequiresDateTime&quot; and @value=&quot;0&quot; ]</value>
   </webElementProperties>
</WebElementEntity>
