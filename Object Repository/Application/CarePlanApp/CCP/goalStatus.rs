<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>goalStatus</name>
   <tag></tag>
   <elementGuidId>5b4f1626-4064-4d05-a702-4c49cc763a08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@name='goal_status']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@name='goal_status']</value>
   </webElementProperties>
</WebElementEntity>
