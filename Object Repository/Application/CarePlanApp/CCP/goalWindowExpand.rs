<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>goalWindowExpand</name>
   <tag></tag>
   <elementGuidId>50392c23-6c62-4e1c-a69d-7f19f15c41a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[contains(text(),&quot;Goals&quot;)and not(contains(text(),&quot;(0)&quot;))]/preceding-sibling::td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),&quot;Goals&quot;)and not(contains(text(),&quot;(0)&quot;))]/preceding-sibling::td</value>
   </webElementProperties>
</WebElementEntity>
